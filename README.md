# kepler_archives

#### archives for kepler installation

Contained in this repository are archives which are referenced for download by puppet installation of coesra kepler.
The nimrod archives are unaltered and contain only build sources - see LICENCE_Nimrod.
Other archives licenced under LICENCE.
